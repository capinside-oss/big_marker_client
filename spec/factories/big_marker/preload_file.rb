FactoryBot.define do
  factory :big_marker_preload_file, class: "BigMarkerClient::Models::PreloadFile" do
    id { SecureRandom.hex(6) }
    sequence(:file_name) { |n| "presentation_#{n}.pdf" }
    file_type { "whiteboard" }
    file_url { "https://host.example.com/content_object_shared_files/some_hash/#{file_name}{?token" }

    initialize_with { new(id: id, file_name: file_name, file_type: file_type, file_url: file_url) }
  end
end
