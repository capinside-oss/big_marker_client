FactoryBot.define do
  factory :big_marker_conference, class: "BigMarkerClient::Models::Conference" do
    transient do
      start_time_obj { Time.now + (3 * BigMarkerClient::TestSupport::DAYS) }
    end

    id { SecureRandom.hex(6) }
    sequence(:title) { |n| "BigMarker Conference #{n}" }
    event_type { "webinar" }
    display_language { "German(Customized)" }
    meeting_mode { false }
    type { "live_webinar" }
    conference_copy_id { SecureRandom.hex(6) }
    master_webinar_id { nil }
    max_attendance { 500 }
    purpose { "BigMarker event description" }
    start_time { start_time_obj.strftime("%FT%T%:z") }
    duration_minutes { 60 }
    sequence(:conference_address) { |n| "https://host.example.com/example_channel/event-#{n}" }
    custom_event_id { nil }
    banner_filter_percentage { Random.rand.round(2) }
    channel_id { "example_channel" }
    webcast_mode { "automatic" }
    end_time { (start_time_obj + (duration_minutes * BigMarkerClient::TestSupport::MINUTES)).strftime("%FT%T%:z") }
    moderator_open_time { (start_time_obj - (30 * BigMarkerClient::TestSupport::MINUTES)).strftime("%FT%T%:z") }
    audience_open_time { (start_time_obj - (15 * BigMarkerClient::TestSupport::MINUTES)).strftime("%FT%T%:z") }
    first_admin_enter_time { (start_time_obj - (60 * BigMarkerClient::TestSupport::MINUTES)).strftime("%FT%T%:z") }
    manual_end_time { nil }
    privacy { "public" }
    exit_url { "https://host.example.com" }
    enable_registration_email { true }
    enable_knock_to_enter { false }
    send_reminder_emails_to_presenters { true }
    review_emails { true }
    poll_results { false }
    enable_ie_safari { true }
    enable_twitter { true }
    auto_invite_all_channel_members { false }
    send_cancellation_email { false }
    show_reviews { false }
    registration_required_to_view_recording { true }
    who_can_watch_recording { "everyone" }
    show_handout_on_page { true }
    background_image_url { nil }
    room_logo { nil }
    agenda_topics { [] }
    disclaimer { "disclaimer" }
    recorded { false }
    recording_url { nil }
    recording_iframe { nil }
    sequence(:sub_url) { |n| "event-#{n}" }
    enable_dial_in { true }
    time_zone { "Europe/Berlin" }
    schedule_type { "one_time" }
    recurring_start_times { [] }
    presenter_advanced_enter_time { 60 }
    attendee_advanced_enter_time { 15 }
    webinar_format { "webinar" }
    registration_conf_emails { true }
    send_notification_emails_to_presenters { true }
    webhook_url { nil }
    channel_admin_id { nil }
    room_type { nil }
    room_sub_title { nil }
    ondemand_video_url { nil }
    presenter_exit_url { nil }
    fb_open_graph_image_url { nil }
    webinar_tags { [] }
    icon { nil }
    dial_in_information { nil }
    preload_files { [] }
    presenters { [] }
    webinar_stats { nil }
    associated_series { [] }

    initialize_with do
      new(id: id, title: title, event_type: event_type, display_language: display_language, meeting_mode: meeting_mode,
          type: type, conference_copy_id: conference_copy_id, master_webinar_id: master_webinar_id,
          max_attendance: max_attendance, purpose: purpose, start_time: start_time, duration_minutes: duration_minutes,
          channel_id: channel_id, conference_address: conference_address, custom_event_id: custom_event_id,
          banner_filter_percentage: banner_filter_percentage, presenter_exit_url: presenter_exit_url,
          webcast_mode: webcast_mode, end_time: end_time, moderator_open_time: moderator_open_time,
          audience_open_time: audience_open_time, first_admin_enter_time: first_admin_enter_time,
          manual_end_time: manual_end_time, privacy: privacy, exit_url: exit_url,
          enable_registration_email: enable_registration_email, enable_knock_to_enter: enable_knock_to_enter,
          review_emails: review_emails, send_reminder_emails_to_presenters: send_reminder_emails_to_presenters,
          poll_results: poll_results, enable_ie_safari: enable_ie_safari, enable_twitter: enable_twitter,
          auto_invite_all_channel_members: auto_invite_all_channel_members, icon: icon,
          send_cancellation_email: send_cancellation_email, show_reviews: show_reviews,
          registration_required_to_view_recording: registration_required_to_view_recording,
          who_can_watch_recording: who_can_watch_recording, show_handout_on_page: show_handout_on_page,
          background_image_url: background_image_url, room_logo: room_logo, agenda_topics: agenda_topics,
          disclaimer: disclaimer, recorded: recorded, recording_url: recording_url, recording_iframe: recording_iframe,
          sub_url: sub_url, enable_dial_in: enable_dial_in, time_zone: time_zone, schedule_type: schedule_type,
          recurring_start_times: recurring_start_times, presenter_advanced_enter_time: presenter_advanced_enter_time,
          attendee_advanced_enter_time: attendee_advanced_enter_time, webinar_format: webinar_format,
          registration_conf_emails: registration_conf_emails, webinar_tags: webinar_tags,
          send_notification_emails_to_presenters: send_notification_emails_to_presenters,
          webhook_url: webhook_url, channel_admin_id: channel_admin_id, ondemand_video_url: ondemand_video_url,
          room_type: room_type, room_sub_title: room_sub_title, dial_in_information: dial_in_information,
          preload_files: preload_files, presenters: presenters, webinar_stats: webinar_stats,
          associated_series: associated_series, fb_open_graph_image_url: fb_open_graph_image_url)
    end

    trait :with_dial_in_information do
      dial_in_information { build(:big_marker_dial_in_information).to_h }
    end

    trait :with_preload_files do
      preload_files { build_list(:big_marker_preload_file, 2).map(&:to_h) }
    end

    trait :with_presenters do
      presenters { build_list(:big_marker_presenter, 2, conference_id: id).map(&:to_h) }
    end

    trait :with_webinar_stats do
      webinar_stats { build(:big_marker_webinar_stats).to_h }
    end
  end
end
