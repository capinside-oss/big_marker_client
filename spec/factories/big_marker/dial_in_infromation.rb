FactoryBot.define do
  factory :big_marker_dial_in_information, class: "BigMarkerClient::Models::DialInInformation" do
    dial_in_number { "+1 (312) 248-9348" }
    dial_in_id { "123456#" }
    dial_in_passcode { "1234#" }
    presenter_dial_in_number { "+1 (312) 248-9348" }
    presenter_dial_in_id { "234567#" }
    presenter_dial_in_passcode { "345678#" }

    initialize_with do
      new(dial_in_number: dial_in_number, dial_in_id: dial_in_id, dial_in_passcode: dial_in_passcode,
          presenter_dial_in_number: presenter_dial_in_number, presenter_dial_in_id: presenter_dial_in_id,
          presenter_dial_in_passcode: presenter_dial_in_passcode)
    end
  end
end
