FactoryBot.define do
  factory :big_marker_webinar_stats, class: "BigMarkerClient::Models::WebinarStats" do
    registrants { 10 }
    revenue { "$0.00" }
    total_attendees { 5 }
    page_views { 4 }
    invited { 20 }

    initialize_with do
      new(registrants: registrants, revenue: revenue, total_attendees: total_attendees, page_views: page_views,
          invited: invited)
    end
  end
end
