FactoryBot.define do
  factory :big_marker_presenter, class: "BigMarkerClient::Models::Presenter" do
    presenter_id { SecureRandom.hex(6) }
    member_id { SecureRandom.hex(6) }
    conference_id { SecureRandom.hex(6) }
    name { "Presenter first name Presenter last name" }
    display_on_landing_page { true }
    presenter_first_name { "Presenter first name" }
    presenter_last_name { "Presenter last name" }
    sequence(:presenter_email) { |n| "presenter.email#{n}@example.com" }
    presenter_url { "https://host.example.com/conferences/#{conference_id}/presenter_content/#{presenter_id}" }
    photo_url { "https://host.example.com/presenters/some_id/medium/presenter_photo.jpg?token" }
    presenter_dial_in_number { "+1 (312) 248-9348" }
    presenter_dial_in_id { "234567#" }
    presenter_dial_in_passcode { "345678#" }
    title { "Presenter title" }
    bio { "Presenter bio" }
    can_manage { false }
    is_moderator { true }
    facebook { nil }
    twitter { nil }
    linkedin { nil }
    website { "https://www.example.com" }
    presenter_temporary_password { "secret" }
    send_email_invite { true }
    add_to_channel_subscriber_list { false }
    enter_as_attendee { false }
    remove_bigmarker_contact_option { true }
    show_on_networking_center { true }
    show_chat_icon_in_booth_or_session_room { true }

    initialize_with do
      new(presenter_id: presenter_id, member_id: member_id, conference_id: conference_id, name: name,
          display_on_landing_page: display_on_landing_page, presenter_first_name: presenter_first_name,
          presenter_last_name: presenter_last_name, presenter_email: presenter_email, presenter_url: presenter_url,
          photo_url: photo_url, presenter_dial_in_number: presenter_dial_in_number,
          presenter_dial_in_id: presenter_dial_in_id, presenter_dial_in_passcode: presenter_dial_in_passcode,
          title: title, bio: bio, can_manage: can_manage, is_moderator: is_moderator, facebook: facebook,
          twitter: twitter, linkedin: linkedin, website: website,
          presenter_temporary_password: presenter_temporary_password, send_email_invite: send_email_invite,
          add_to_channel_subscriber_list: add_to_channel_subscriber_list, enter_as_attendee: enter_as_attendee,
          remove_bigmarker_contact_option: remove_bigmarker_contact_option,
          show_on_networking_center: show_on_networking_center,
          show_chat_icon_in_booth_or_session_room: show_chat_icon_in_booth_or_session_room)
    end
  end
end
