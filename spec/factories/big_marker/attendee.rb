FactoryBot.define do
  factory :big_marker_attendee, class: "BigMarkerClient::Models::Attendee" do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    sequence(:bmid) { SecureRandom.hex(6) }
    sequence(:conference_id) { SecureRandom.hex(6) }
    sequence(:email) { |n| "attendee#{n}@example.com" }
    first_name { "Attendee first name" }
    last_name { "Attendee last name" }
    edited_in_room_display_name { nil }
    custom_fields { [] }
    entered_at { (Time.now - (60 * BigMarkerClient::TestSupport::MINUTES)).strftime("%FT%T%:z") }
    leaved_at { Time.now.strftime("%FT%T%:z") }
    total_duration { Time.at(rand * 120 * 60).utc.strftime("%T") }
    engaged_duration { Time.at(rand * 120 * 60).utc.strftime("%T") }
    attendance_monitor { { "show_popup" => 0, "click_popup" => 0 } }
    survey_results { [] }
    time_zone { "Europe/Berlin" }
    country { "Germany" }
    chats_count { 1 }
    qas_count { 0 }
    polls_count { 2 }
    polls { %w[poll1 poll2] }
    questions { [] }
    handouts { %w[doc1 doc2 doc3] }
    browser_name { "Google Chrome" }
    browser_version { 96 }
    device_name { "desktop" }
    download_handout { [] }
    view_handout { [] }
    certificate_of_completion { "" }
    start_time { (Time.now - (45 * BigMarkerClient::TestSupport::MINUTES)).strftime("%FT%T%:z") }
    coupon_used { nil }

    initialize_with do
      new(id: id, bmid: bmid, conference_id: conference_id, email: email, first_name: first_name, last_name: last_name,
          edited_in_room_display_name: edited_in_room_display_name, custom_fields: custom_fields,
          entered_at: entered_at, leaved_at: leaved_at, total_duration: total_duration, start_time: start_time,
          engaged_duration: engaged_duration, attendance_monitor: attendance_monitor, survey_results: survey_results,
          time_zone: time_zone, country: country, chats_count: chats_count, qas_count: qas_count,
          polls_count: polls_count, polls: polls, questions: questions, handouts: handouts, browser_name: browser_name,
          browser_version: browser_version, device_name: device_name, download_handout: download_handout,
          view_handout: view_handout, certificate_of_completion: certificate_of_completion, coupon_used: coupon_used)
    end

    trait :with_view_handouts do
      view_handout { build_list(:big_marker_handout, 1, :view_handout) }
    end

    trait :with_download_handouts do
      download_handout { build_list(:big_marker_handout, 1, :download_handout) }
    end
  end
end
