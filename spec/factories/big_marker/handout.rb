FactoryBot.define do
  factory :big_marker_handout, class: "BigMarkerClient::Models::Handout" do
    type { "handout" }
    sequence(:email) { |n| "attendee-email#{n}@example.com" }
    sequence(:content) { |n| "some_file_name#{n}.pdf" }
    timestamp { Time.now.strftime("%m/%d/%Y %H:%M:%S %z") }
    source_from { "Live Webinar" }

    trait :view_handout do
      type { "view_handout" }
    end

    trait :download_handout do
      type { "download_handout" }
    end

    initialize_with do
      new(type: type, email: email, content: content, timestamp: timestamp, source_from: source_from)
    end
  end
end
