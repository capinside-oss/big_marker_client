FactoryBot.define do
  factory :big_marker_registrant, class: "BigMarkerClient::Models::Registrant" do
    sequence(:email) { |n| "registrant.email#{n}@example.com" }
    first_name { "Registrant first name" }
    last_name { "Registrant last name" }
    custom_fields { [] }
    enter_url { "https://host.example.com/example_channel/event?bmid=#{bmid}" }
    bmid { SecureRandom.hex(6) }
    referral_domain { nil }
    source { "Registration Modal" }
    tracking_code { nil }
    custom_user_id { nil }
    time_zone { "Europe/Berlin" }
    country { "Germany" }
    earned_certificate { true }
    qualified_for_certificate { true }

    initialize_with do
      new(email: email, first_name: first_name, last_name: last_name, custom_fields: custom_fields,
          enter_url: enter_url, bmid: bmid, referral_domain: referral_domain, source: source,
          tracking_code: tracking_code, custom_user_id: custom_user_id, time_zone: time_zone, country: country,
          earned_certificate: earned_certificate, qualified_for_certificate: qualified_for_certificate)
    end
  end
end
