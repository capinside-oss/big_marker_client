FactoryBot.define do
  factory :big_marker_survey_result, class: "BigMarkerClient::Models::SurveyResult" do
    sequence(:question) { |n| "question #{n}" }
    sequence(:email) { |n| "attendee-email#{n}@example.com" }
    date { Date.today.strftime("%a, %b %d, %Y") }
    response { "response" }

    initialize_with do
      new(question: question, email: email, date: date, response: response)
    end
  end
end
