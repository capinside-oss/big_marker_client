require "spec_helper"

RSpec.describe BigMarkerClient::Api::V1::Conference do
  let(:conference) { CONFERENCE_JSON }
  let(:conference_model) { BigMarkerClient::Models::Conference.new(conference) }
  let(:attendees_hash) do
    { "attendees" => [
      {
        email: "example.user1@bigmarker.com",
        first_name: "Example",
        last_name: "user"
      },
      {
        email: "another.registrant2@bigmarker.com",
        first_name: "Another",
        last_name: "Registrant"
      }
    ] }
  end

  before do
    BigMarkerClient::Config.api_key = "ABC123abc"
  end

  describe "#list" do
    before do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "conferences" => [conference] })
    end

    it "succeeds" do
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences", {}).once
      result = described_class.list
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.id).to eq conference_model.id
      expect(result.first.preload_files).to all(be_instance_of(BigMarkerClient::Models::PreloadFile))
    end

    it "can search" do
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences", { query: "Practice" }).once
      result = described_class.list({ query: "Practice" })
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.title).to eq "Practice session with presenter"
      expect(result.first.dial_in_information).to be_instance_of(BigMarkerClient::Models::DialInInformation)
    end
  end

  describe "#list_all" do
    before do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "conferences" => [conference], "total_pages" => 1,
                                                                 "total_count" => 1 })
    end

    it "succeeds" do
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences", { "page" => 1 }).once
      result = described_class.list_all
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.id).to eq conference_model.id
      expect(result.first.preload_files).to all(be_instance_of(BigMarkerClient::Models::PreloadFile))
    end
  end

  describe "#search" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:post).and_return({ "conferences" => [conference] })
      expect(BigMarkerClient::Base).to receive(:post).with("/conferences/search", { start_time: 1_609_556_645,
                                                                                    end_time: 1_623_053_350 }).once
      result = described_class.search({ start_time: Time.utc(2021, 1, 2, 3, 4, 5).to_i,
                                        end_time: Time.utc(2021, 6, 7, 8, 9, 10).to_i })
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.event_type).to eq "webinar"
      expect(result.first.presenters).to all(be_instance_of(BigMarkerClient::Models::Presenter))
    end
  end

  describe "#search_all" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:post).and_return({ "conferences" => [conference], "total_pages" => 1,
                                                                  "total_entries" => 1 })
      expect(BigMarkerClient::Base).to receive(:post)
        .with("/conferences/search", { start_time: 1_609_556_645, end_time: 1_623_053_350, "page" => 1 }).once
      result = described_class.search_all({ start_time: Time.utc(2021, 1, 2, 3, 4, 5).to_i,
                                            end_time: Time.utc(2021, 6, 7, 8, 9, 10).to_i })
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.event_type).to eq "webinar"
      expect(result.first.presenters).to all(be_instance_of(BigMarkerClient::Models::Presenter))
    end
  end

  describe "#show" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "conference" => conference })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/bfd2422502b8").once
      result = described_class.show(conference_model.id)
      expect(result).to be_instance_of(BigMarkerClient::Models::Conference)
      expect(result.recorded).to be_falsey
      expect(result.webinar_stats).to be_instance_of(BigMarkerClient::Models::WebinarStats)
    end
  end

  describe "#associated_series" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "conferences" => [conference] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/get_associated_sessions/bfd2422502b8", {}).once
      result = described_class.associated_series(conference_model.id)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.disclaimer).to be_nil
      expect(result.first.agenda_topics).to eq []
    end
  end

  describe "#associated_series_all" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "conferences" => [conference], "total_pages" => 1,
                                                                 "total_entries" => 1 })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/get_associated_sessions/bfd2422502b8",
                                                          { "page" => 1 }).once
      result = described_class.associated_series_all(conference_model.id)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.disclaimer).to be_nil
      expect(result.first.agenda_topics).to eq []
    end
  end

  describe "#recurring" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "child_conferences" => [conference] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/recurring/bfd2422502b8",
                                                          { end_time: be_instance_of(Integer) }).once
      result = described_class.recurring(conference_model.id, end_time: Time.now.to_i)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.room_logo).to match(/conference_bigroom_logo/)
      expect(result.first.background_image_url).to match(/conference_background_images/)
    end
  end

  describe "#recurring_all" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "child_conferences" => [conference],
                                                                 "total_pages" => 1, "total_entries" => 1 })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/recurring/bfd2422502b8",
                                                          { end_time: be_instance_of(Integer), "page" => 1 }).once
      result = described_class.recurring_all(conference_model.id, end_time: Time.now.to_i)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
      expect(result.first.room_logo).to match(/conference_bigroom_logo/)
      expect(result.first.background_image_url).to match(/conference_background_images/)
    end
  end

  describe "#update" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:put).and_return(conference)
      expect(BigMarkerClient::Base).to receive(:put).with("/conferences/bfd2422502b8", { title: "new title" }).once
      result = described_class.update(conference_model.id, { title: "new title" })
      expect(result).to be_instance_of(BigMarkerClient::Models::Conference)
      expect(result.who_can_watch_recording).to eq "channel_admin_only"
      expect(result.show_handout_on_page).to be_falsey
    end
  end

  describe "#create" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:post).and_return(conference)
      expect(BigMarkerClient::Base).to receive(:post).with("/conferences", { title: "new title" }).once
      result = described_class.create({ title: "new title" })
      expect(result).to be_instance_of(BigMarkerClient::Models::Conference)
      expect(result.show_reviews).to be_truthy
      expect(result.registration_required_to_view_recording).to be_truthy
    end
  end

  describe "#destroy" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:delete).and_return(nil)
      expect(BigMarkerClient::Base).to receive(:delete).with("/conferences/bfd2422502b8").once
      result = described_class.destroy(conference_model.id)
      expect(result).to be_nil
    end
  end

  describe "#enter" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:post).and_return({ "enter_token" => "foo", "enter_uri" => "bar" })
      expect(BigMarkerClient::Base).to receive(:post).with("/conferences/enter",
                                                           { id: "bfd2422502b8", attendee_name: "John Doe",
                                                             attendee_email: "john.doe@example.com" }).once
      result = described_class.enter(conference["id"], "John Doe", "john.doe@example.com")
      expect(result).to be_instance_of(Hash)
      expect(result.size).to eq 2
      expect(result["enter_token"]).to eq "foo"
      expect(result["enter_uri"]).to eq "bar"
    end
  end

  describe "#presenters" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "presenters" => conference["presenters"] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/bfd2422502b8/presenters").once
      result = described_class.presenters(conference_model.id)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Presenter))
      expect(result.length).to eq 1
      expect(result.first.display_name).to eq "First name Last name"
      expect(result.first.presenter_url).to match(/presenter_content/)
    end
  end

  describe "#attendees" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return(attendees_hash)
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/bfd2422502b8/attendees", {}).once
      result = described_class.attendees(conference_model.id)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Attendee))
      expect(result.length).to eq 2
      expect(result.first.email).to eq "example.user1@bigmarker.com"
      expect(result.last.email).to eq "another.registrant2@bigmarker.com"
    end
  end

  describe "#attendees_all" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return(**attendees_hash)
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/bfd2422502b8/attendees", { "page" => 1 }).once
      result = described_class.attendees_all(conference_model.id)
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Attendee))
      expect(result.length).to eq 2
      expect(result.first.email).to eq "example.user1@bigmarker.com"
      expect(result.last.email).to eq "another.registrant2@bigmarker.com"
    end
  end
end
