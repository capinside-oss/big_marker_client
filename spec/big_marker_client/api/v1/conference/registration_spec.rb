require "spec_helper"

RSpec.describe BigMarkerClient::Api::V1::Conference::Registration do
  let(:conference) { CONFERENCE_JSON }
  let(:registrant) { REGISTRANT_JSON }

  before do
    BigMarkerClient::Config.api_key = "ABC123abc"
  end

  describe "#list" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "registrations" => [registrant] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/registrations/bfd2422502b8", {})
      result = described_class.list(conference["id"])
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Registrant))
      expect(result.length).to eq 1
      expect(result.first.first_name).to eq registrant["first_name"]
      expect(result.first.source).to eq "Invitation Email"
    end
  end

  describe "#list_all" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "registrations" => [registrant] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/registrations/bfd2422502b8", { "page" => 1 })
      result = described_class.list_all(conference["id"])
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Registrant))
      expect(result.length).to eq 1
      expect(result.first.first_name).to eq registrant["first_name"]
      expect(result.first.source).to eq "Invitation Email"
    end
  end

  describe "#list_with_fields" do
    let(:registrant_with_fields) { registrant.dup }

    before do
      registrant_with_fields["custom_fields"] = { "key1" => "value1", "key2" => "value2" }
    end

    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "registrations" => [registrant_with_fields] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/registrations_with_fields/bfd2422502b8", {})
      result = described_class.list_with_fields(conference["id"])
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Registrant))
      expect(result.length).to eq 1
      expect(result.first.first_name).to eq registrant["first_name"]
      expect(result.first.custom_fields).to eq({ "key1" => "value1", "key2" => "value2" })
    end
  end

  describe "#list_alll_with_fields" do
    let(:registrant_with_fields) { registrant.dup }

    before do
      registrant_with_fields["custom_fields"] = { "key1" => "value1", "key2" => "value2" }
    end

    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "registrations" => [registrant_with_fields] })
      expect(BigMarkerClient::Base).to receive(:get).with("/conferences/registrations_with_fields/bfd2422502b8",
                                                          { "page" => 1 })
      result = described_class.list_all_with_fields(conference["id"])
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Registrant))
      expect(result.length).to eq 1
      expect(result.first.first_name).to eq registrant["first_name"]
      expect(result.first.custom_fields).to eq({ "key1" => "value1", "key2" => "value2" })
    end
  end

  describe "#register" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:post)
        .and_return({ "conference_url" => "https://www.bigmarker.com/example/some-event?bmid=33d73a52626c" })
      expect(BigMarkerClient::Base).to receive(:post)
        .with("/conferences/register", { email: "registrant@example.com", first_name: "Registrant first",
                                         id: "bfd2422502b8", last_name: "Registrant last" })
      result = described_class.register(conference["id"], registrant["email"], registrant["first_name"],
                                        registrant["last_name"])
      expect(result).to eq "https://www.bigmarker.com/example/some-event?bmid=33d73a52626c"
    end
  end

  describe "#unregister" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:delete).and_return(nil)
      expect(BigMarkerClient::Base).to receive(:delete)
        .with("/conferences/register", { email: "registrant@example.com", id: "bfd2422502b8" })
      result = described_class.unregister(conference["id"], registrant["email"])
      expect(result).to be_nil
    end
  end

  describe "#check_status" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "registration_status" => "registered" })
      expect(BigMarkerClient::Base).to receive(:get)
        .with("/conferences/bfd2422502b8/check_registration_status", { email: "registrant@example.com" })
      result = described_class.check_status(conference["id"], email: registrant["email"])
      expect(result).to eq "registered"
    end
  end
end
