require "spec_helper"

RSpec.describe BigMarkerClient::Api::V1::Presenter do
  let(:conference) { CONFERENCE_JSON }
  let(:presenter) { PRESENTER_JSON }

  before do
    BigMarkerClient::Config.api_key = "ABC123abc"
  end

  describe "#list" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:get).and_return({ "conferences" => [conference] })
      expect(BigMarkerClient::Base).to receive(:get).with("/presenters/c43772f78280/list_conferences",
                                                          { channel_id: "example" })
      result = described_class.list_conferences(presenter["member_id"], conference["channel_id"])
      expect(result).to all(be_instance_of(BigMarkerClient::Models::Conference))
      expect(result.length).to eq 1
    end
  end

  describe "#update" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:put).and_return({ "presenter" => presenter })
      expect(BigMarkerClient::Base).to receive(:put).with("/presenters/c43772f78280", { name: "new name" })
      result = described_class.update(presenter["member_id"], { name: "new name" })
      expect(result).to be_instance_of(BigMarkerClient::Models::Presenter)
      expect(result.name).to eq "First name Last name"
      expect(result.website).to eq "https://capinside.com"
    end
  end

  describe "#create" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:post).and_return({ "presenter" => presenter })
      expect(BigMarkerClient::Base).to receive(:post).with("/presenters", { "conference_id" => "bfd2422502b8",
                                                                            "name" => "new name" })
      result = described_class.create(conference["id"], { "name" => "new name" })
      expect(result).to be_instance_of(BigMarkerClient::Models::Presenter)
      expect(result.bio).to eq "Example bio"
      expect(result.presenter_image_url).to match(/member_photos/)
    end
  end

  describe "#destroy" do
    it "succeeds" do
      allow(BigMarkerClient::Base).to receive(:delete).and_return(nil)
      expect(BigMarkerClient::Base).to receive(:delete).with("/presenters/c43772f78280")
      result = described_class.destroy(presenter["member_id"])
      expect(result).to be_nil
    end
  end
end
