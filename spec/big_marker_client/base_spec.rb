require "spec_helper"

RSpec.describe BigMarkerClient::Base do
  context "with api key" do
    before do
      BigMarkerClient::Config.api_key = "ABC123abc"
    end

    describe "#post" do
      before do
        stub_request(:post, "https://www.bigmarker.com/api/v1/test").to_return(status: 200, body: "POST".to_json)
      end

      it "calls BigMarker with the correct parameters" do
        expect(described_class.post("/test", { attr: "value" })).to eq "POST"
        expect(WebMock).to have_requested(:post, "https://www.bigmarker.com/api/v1/test")
          .with(body: "{\"attr\":\"value\"}",
                headers: { "Api-Key" => "ABC123abc", "Content-Type" => "application/json" }).once
      end

      it "works with paths starting without slash" do
        expect(described_class.post("test", { attr: "value" })).to eq "POST"
        expect(WebMock).to have_requested(:post, "https://www.bigmarker.com/api/v1/test")
          .with(body: "{\"attr\":\"value\"}",
                headers: { "Api-Key" => "ABC123abc", "Content-Type" => "application/json" }).once
      end

      it "fails without a path" do
        expect do
          described_class.post(nil, { attr: "value" })
        end.to raise_error(ArgumentError, "http_method, path or api key is missing")
      end
    end

    describe "#put" do
      before do
        stub_request(:put, "https://www.bigmarker.com/api/v1/test").to_return(status: 200, body: "PUT".to_json)
      end

      it "calls BigMarker with the correct parameters" do
        expect(described_class.put("/test", { attr: "value" })).to eq "PUT"
        expect(WebMock).to have_requested(:put, "https://www.bigmarker.com/api/v1/test")
          .with(body: "{\"attr\":\"value\"}",
                headers: { "Api-Key" => "ABC123abc", "Content-Type" => "application/json" }).once
      end
    end

    describe "#patch" do
      before do
        stub_request(:patch, "https://www.bigmarker.com/api/v1/test").to_return(status: 200, body: "PATCH".to_json)
      end

      it "calls BigMarker with the correct parameters" do
        expect(described_class.patch("/test", { attr: "value" })).to eq "PATCH"
        expect(WebMock).to have_requested(:patch, "https://www.bigmarker.com/api/v1/test")
          .with(body: "{\"attr\":\"value\"}",
                headers: { "Api-Key" => "ABC123abc", "Content-Type" => "application/json" }).once
      end
    end

    describe "#delete" do
      before do
        stub_request(:delete, "https://www.bigmarker.com/api/v1/test").to_return(status: 200, body: "")
      end

      it "calls BigMarker with the correct parameters" do
        expect(described_class.delete("/test")).to be_nil
        expect(WebMock).to have_requested(:delete, "https://www.bigmarker.com/api/v1/test")
          .with(headers: { "Api-Key" => "ABC123abc", "Content-Type" => "application/json" }).once
      end

      it "returns nil ith body is blank" do
        expect(described_class.delete("/test")).to be_nil
      end
    end

    describe "#get" do
      before do
        stub_request(:get, "https://www.bigmarker.com/api/v1/test?attr=value").to_return(status: 200, body: "[]")
      end

      it "calls BigMarker with the correct parameters" do
        expect(described_class.get("/test", { attr: "value" })).to eq []
        expect(WebMock).to have_requested(:get, "https://www.bigmarker.com/api/v1/test")
          .with(query: { attr: "value" },
                headers: { "Api-Key" => "ABC123abc", "Content-Type" => "application/json" }).once
      end
    end

    context "with logging" do
      before do
        BigMarkerClient::Config.log = true
        described_class.instance_variable_set(:@http_client, nil)
        stub_request(:get, "https://www.bigmarker.com/api/v1/test?password=secret")
          .to_return(status: 200, body: "GET".to_json)
      end

      after do
        BigMarkerClient::Config.log = false
      end

      it "filters passwords and api keys" do
        expect do
          expect(described_class.get("/test", { password: "secret" })).to eq "GET"
        end.to output(/.*password=\[FILTERED\]\n.*\nAPI-KEY: \[FILTERED\]/).to_stdout
      end
    end

    context "with invalid response" do
      before do
        stub_request(:get, "https://www.bigmarker.com/api/v1/test")
          .to_return(status: 200, body: "INVALID")
      end

      it "raises an exception" do
        expect do
          described_class.get("/test")
        end.to raise_error(BigMarkerClient::ResponseError, "invalid response")
      end
    end
  end

  context "without api key" do
    before do
      BigMarkerClient::Config.api_key = nil
    end

    describe "#post" do
      it "fails without API key" do
        expect do
          described_class.post("/test")
        end.to raise_error(ArgumentError, "http_method, path or api key is missing")
      end
    end
  end
end
