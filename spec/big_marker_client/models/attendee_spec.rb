require "spec_helper"

RSpec.describe BigMarkerClient::Models::Attendee, type: :model do
  let(:attendee) { build(:big_marker_attendee) }
  let(:expected_hash) do
    { "browser_name" => "Google Chrome",
      "browser_version" => 96,
      "chats_count" => 1,
      "device_name" => "desktop",
      "email" => attendee.email,
      "engaged_duration" => attendee.engaged_duration,
      "first_name" => "Attendee first name",
      "handouts" => %w[doc1 doc2 doc3],
      "last_name" => "Attendee last name",
      "polls" => %w[poll1 poll2],
      "polls_count" => 2,
      "qas_count" => 0,
      "questions" => [],
      "total_duration" => attendee.total_duration }
  end

  it "has a valid factory" do
    expect(attendee.to_h).to include(expected_hash)
    expect(attendee.entered_at).not_to be_nil
    expect(attendee.leaved_at).not_to be_nil
  end
end
