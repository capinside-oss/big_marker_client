require "spec_helper"

RSpec.describe BigMarkerClient::Models::Registrant, type: :model do
  let(:registrant) { build(:big_marker_registrant) }
  let(:expected_hash) do
    { "bmid" => registrant.bmid,
      +"country" => "Germany",
      +"custom_fields" => [],
      +"custom_user_id" => nil,
      +"email" => registrant.email,
      +"enter_url" => "https://host.example.com/example_channel/event?bmid=#{registrant.bmid}",
      +"first_name" => "Registrant first name",
      +"last_name" => "Registrant last name",
      +"referral_domain" => nil,
      +"source" => "Registration Modal",
      +"time_zone" => "Europe/Berlin",
      +"tracking_code" => nil }
  end

  it "has a valid factory" do
    expect(registrant.to_h).to include(expected_hash)
  end
end
