require "spec_helper"

RSpec.describe BigMarkerClient::Models::Presenter, type: :model do
  let(:presenter) { build(:big_marker_presenter) }
  let(:expected_hash) do
    { "add_to_channel_subscriber_list" => false,
      "bio" => "Presenter bio",
      "can_manage" => false,
      "conference_id" => presenter.conference_id,
      "display_on_landing_page" => true,
      "facebook" => nil,
      "is_moderator" => true,
      "linkedin" => nil,
      "member_id" => presenter.member_id,
      "name" => "Presenter first name Presenter last name",
      "photo_url" => "https://host.example.com/presenters/some_id/medium/presenter_photo.jpg?token",
      "presenter_dial_in_id" => "234567#",
      "presenter_dial_in_number" => "+1 (312) 248-9348",
      "presenter_dial_in_passcode" => "345678#",
      "presenter_email" => presenter.email,
      "presenter_first_name" => "Presenter first name",
      "presenter_id" => presenter.presenter_id,
      "presenter_last_name" => "Presenter last name",
      "presenter_temporary_password" => "secret",
      "presenter_url" =>
        "https://host.example.com/conferences/#{presenter.conference_id}/presenter_content/#{presenter.presenter_id}",
      "send_email_invite" => true,
      "title" => "Presenter title",
      "twitter" => nil,
      "website" => "https://www.example.com" }
  end

  it "has a valid factory" do
    expect(presenter.to_h).to include(expected_hash)
  end
end
