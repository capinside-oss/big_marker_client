require "spec_helper"

RSpec.describe BigMarkerClient::Models::PreloadFile, type: :model do
  let(:preload_file) { build(:big_marker_preload_file) }
  let(:expected_hash) do
    { "file_name" => preload_file.file_name,
      "file_type" => "whiteboard",
      "file_url" => preload_file.file_url,
      "id" => preload_file.id }
  end

  it "has a valid factory" do
    expect(preload_file.to_h).to include(expected_hash)
  end
end
