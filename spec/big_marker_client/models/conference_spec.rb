require "spec_helper"

RSpec.describe BigMarkerClient::Models::Conference, type: :model do
  let(:conference) do
    build(:big_marker_conference, :with_dial_in_information, :with_preload_files, :with_presenters, :with_webinar_stats)
  end
  let(:expected_hash) do
    { "agenda_topics" => [],
      "associated_series" => [],
      "attendee_advanced_enter_time" => 15,
      "auto_invite_all_channel_members" => false,
      "background_image_url" => nil,
      "channel_admin_id" => nil,
      "channel_id" => "example_channel",
      "conference_address" => "https://host.example.com/example_channel/event-1",
      "conference_copy_id" => conference.conference_copy_id,
      "custom_event_id" => nil,
      "dial_in_information" => { "dial_in_id" => "123456#", "dial_in_number" => "+1 (312) 248-9348",
                                 "dial_in_passcode" => "1234#", "presenter_dial_in_id" => "234567#",
                                 "presenter_dial_in_number" => "+1 (312) 248-9348",
                                 "presenter_dial_in_passcode" => "345678#" },
      "disclaimer" => "disclaimer",
      "display_language" => "German(Customized)",
      "duration_minutes" => 60,
      "enable_dial_in" => true,
      "enable_ie_safari" => true,
      "enable_knock_to_enter" => false,
      "enable_registration_email" => true,
      "enable_twitter" => true,
      "event_type" => "webinar",
      "exit_url" => "https://host.example.com",
      "id" => conference.id,
      "manual_end_time" => nil,
      "master_webinar_id" => nil,
      "max_attendance" => 500,
      "meeting_mode" => false,
      "poll_results" => false,
      "presenter_advanced_enter_time" => 60,
      "privacy" => "public",
      "purpose" => "BigMarker event description",
      "recorded" => false,
      "recording_iframe" => nil,
      "recording_url" => nil,
      "recurring_start_times" => [],
      "registration_conf_emails" => true,
      "registration_required_to_view_recording" => true,
      "review_emails" => true,
      "room_logo" => nil,
      "room_sub_title" => nil,
      "room_type" => nil,
      "schedule_type" => "one_time",
      "send_cancellation_email" => false,
      "send_notification_emails_to_presenters" => true,
      "send_reminder_emails_to_presenters" => true,
      "show_handout_on_page" => true,
      "show_reviews" => false,
      "sub_url" => "event-1",
      "time_zone" => "Europe/Berlin",
      "title" => "BigMarker Conference 1",
      "type" => "live_webinar",
      "webcast_mode" => "automatic",
      "webhook_url" => nil,
      "webinar_format" => "webinar",
      "webinar_stats" =>
        { "invited" => 20, "page_views" => 4, "registrants" => 10, "revenue" => "$0.00", "total_attendees" => 5 },
      "who_can_watch_recording" => "everyone" }
  end

  it "has a valid factory" do
    expect(conference.to_h).to include(expected_hash)
    expect([conference.audience_open_time, conference.end_time, conference.first_admin_enter_time,
            conference.moderator_open_time, conference.start_time]).to all(match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/))
    expect(conference.presenters.size).to eq 2
    expect(conference.preload_files.size).to eq 2
  end
end
