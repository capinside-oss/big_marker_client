require "spec_helper"

RSpec.describe BigMarkerClient::Models::WebinarStats, type: :model do
  let(:webinar_stats) { build(:big_marker_webinar_stats) }
  let(:expected_hash) do
    { "invited" => 20, "page_views" => 4, "registrants" => 10, "revenue" => "$0.00", "total_attendees" => 5 }
  end

  it "has a valid factory" do
    expect(webinar_stats.to_h).to include(expected_hash)
  end
end
