require "spec_helper"

RSpec.describe BigMarkerClient::Models::DialInInformation, type: :model do
  ##
  # this is used to test BigMarkerClient::Models::Base indirectly
  context "when testing underlying base class" do
    let(:dial_in_info) { CONFERENCE_JSON["dial_in_information"] }

    describe "#initialize" do
      it "creates an object with properties" do
        result = described_class.new(dial_in_info)
        expect(result).not_to be_nil
        expect(result.dial_in_number).to eq "+1 (555) 123-4567"
        expect(result.dial_in_passcode).to eq "1234#"
        expect(result.presenter_dial_in_number).to eq "+1 (555) 234-5678"
        expect(result.presenter_dial_in_passcode).to eq "123456#"
      end
    end

    describe "#to_h" do
      it "creates a hash from an object" do
        result = described_class.new(dial_in_info).to_h
        expect(result).not_to be_nil
        expect(result).to eq({ "dial_in_id" => "", "dial_in_number" => "+1 (555) 123-4567",
                               "dial_in_passcode" => "1234#", "presenter_dial_in_id" => "#",
                               "presenter_dial_in_number" => "+1 (555) 234-5678",
                               "presenter_dial_in_passcode" => "123456#" })
      end
    end
  end

  context "when testing the factory" do
    let(:dial_in_info) { build(:big_marker_dial_in_information) }
    let(:expected_hash) do
      { "dial_in_id" => "123456#",
        "dial_in_number" => "+1 (312) 248-9348",
        "dial_in_passcode" => "1234#",
        "presenter_dial_in_id" => "234567#",
        "presenter_dial_in_number" => "+1 (312) 248-9348",
        "presenter_dial_in_passcode" => "345678#" }
    end

    it "has a valid factory" do
      expect(dial_in_info.to_h).to include(expected_hash)
    end
  end
end
