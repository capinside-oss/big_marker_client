# frozen_string_literal: true

require "big_marker_client"
require "rspec"
require "webmock"
require "webmock/rspec"
require "big_marker_client/test_support"
require "factory_bot"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
    c.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.default_formatter = "doc" if config.files_to_run.one?

  config.order = :random
  Kernel.srand config.seed

  # Factory Bot
  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.find_definitions
  end
end

allow_list = %w[localhost 127.0.0.1]
WebMock.disable_net_connect!(allow: ->(uri) { allow_list.any? { |site| uri.host.include?(site) } })

# Just enable Webmock globally otherwise we get interesting problems when it
# is disabled and enabled inconsistently
WebMock.enable!

PRESENTER_JSON = {
  "presenter_id" => "178dbbb2888f",
  "member_id" => "c43772f78280",
  "conference_id" => "bfd2422502b8",
  "display_name" => "First name Last name",
  "display_on_landing_page" => false,
  "presenter_image_url" => "https://d5ln38p3754yc.cloudfront.net/member_photos/21129791/medium/" \
                           "1635870806-118c93552decfc46.png?1635870806",
  "first_name" => "First name",
  "last_name" => "Last name",
  "email" => "presenter@example.com",
  "presenter_url" => "https://www.bigmarker.com/conferences/bfd2422502b8/presenter_content/" \
                     "178dbbb2888f?invited_presenter_id=178dbbb2888f",
  "presenter_dial_in_number" => "+1 (555) 123-4567",
  "presenter_dial_in_id" => "#",
  "presenter_dial_in_passcode" => "123456#",
  "title" => "",
  "bio" => "Example bio",
  "can_manage" => true,
  "is_moderator" => false,
  "facebook" => "https://www.facebook.com/example/",
  "twitter" => "https://twitter.com/example",
  "linkedin" => "https://www.linkedin.com/company/example/mycompany/",
  "website" => "https://capinside.com"
}.freeze

CONFERENCE_JSON = {
  "id" => "bfd2422502b8",
  "title" => "Practice session with presenter",
  "event_type" => "webinar",
  "language" => "German(Customized)",
  "meeting_mode" => false,
  "type" => "live_webinar",
  "copy_webinar_id" => "6405c7198c25",
  "master_webinar_id" => nil,
  "purpose" => "Practice session with presenter",
  "start_time" => "2021-12-06T15:00:00+01:00",
  "duration" => 60,
  "conference_address" => "https://www.bigmarker.com/example/practice-session",
  "custom_event_id" => nil,
  "channel_id" => "example",
  "webcast_mode" => "automatic",
  "end_time" => "2021-12-06T15:20:48+01:00",
  "moderator_open_time" => "2021-12-06T08:00:00-05:00",
  "audience_open_time" => "2021-12-06T08:45:00-05:00",
  "first_admin_enter_time" => "2021-12-06T14:56:14+01:00",
  "manual_end_time" => "2021-12-06T14:20:48+00:00",
  "dial_in_information" => {
    "dial_in_number" => "+1 (555) 123-4567", "dial_in_id" => "", "dial_in_passcode" => "1234#",
    "presenter_dial_in_number" => "+1 (555) 234-5678", "presenter_dial_in_id" => "#",
    "presenter_dial_in_passcode" => "123456#"
  },
  "privacy" => "private",
  "exit_url" => "https://example.com/some/url",
  "enable_registration_email" => true,
  "enable_knock_to_enter" => false,
  "send_reminder_emails_to_presenters" => false,
  "enable_review_emails" => false,
  "can_view_poll_results" => true,
  "enable_ie_safari" => true,
  "enable_twitter" => false,
  "auto_invite_all_channel_members" => true,
  "send_cancellation_email" => true,
  "show_reviews" => true,
  "registration_required_to_view_recording" => true,
  "who_can_watch_recording" => "channel_admin_only",
  "show_handout_on_page" => false,
  "background_image_url" => "https://d5ln38p3754yc.cloudfront.net/conference_background_images/5220625/original/" \
                            "1638454369-aee143f953c05e7f.jpg?1638454369",
  "room_logo" => "https://d5ln38p3754yc.cloudfront.net/conference_bigroom_logo/5220625/original/" \
                 "1638454370-12e61c77ad5b8d3f.png?1638454370",
  "agenda_topics" => [],
  "preload_files" =>
    [
      { "id" => "ea7c47ed90a7",
        "file_name" => "presentation.pdf",
        "file_type" => "whiteboard",
        "file_url" => "https://d5ln38p3754yc.cloudfront.net/content_object_shared_files/" \
                      "54487ac9b768b24e343f75bb863be1140ed52c40/presentation.pdf?1638786903" }
    ],
  "disclaimer" => nil,
  "presenters" => [PRESENTER_JSON],
  "recorded" => false,
  "presenter_exit_url" => nil,
  "fb_open_graph_image_url" => nil,
  "webinar_stats" => {
    "registrants" => 1, "revenue" => "$0.00", "total_attendees" => 0, "page_views" => 5, "invited" => 0
  },
  "associated_series" => []
}.freeze

REGISTRANT_JSON = {
  "email" => "registrant@example.com",
  "custom_user_id" => nil,
  "country" => "Germany",
  "first_name" => "Registrant first",
  "last_name" => "Registrant last",
  "enter_url" => "https://www.bigmarker.com/example/some-event?bmid=25d2022188f2",
  "bmid" => "25d2022188f2",
  "referral_domain" => nil,
  "source" => "Invitation Email",
  "tracking_code" => nil,
  "time_zone" => "Europe/Berlin",
  "earned_certificate" => false,
  "qualified_for_certificate" => false
}.freeze
