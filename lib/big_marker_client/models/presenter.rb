module BigMarkerClient
  module Models
    class Presenter < Base
      attr_accessor :presenter_id, :member_id, :conference_id, :name, :display_on_landing_page, :presenter_first_name,
                    :presenter_last_name, :presenter_email, :presenter_url, :photo_url, :presenter_dial_in_number,
                    :presenter_dial_in_id, :presenter_dial_in_passcode, :title, :bio, :can_manage, :is_moderator,
                    :facebook, :twitter, :linkedin, :website,
                    # for presenter creation
                    :presenter_temporary_password, :send_email_invite, :add_to_channel_subscriber_list,
                    :enter_as_attendee, :remove_bigmarker_contact_option, :show_on_networking_center,
                    :show_chat_icon_in_booth_or_session_room

      # differences between creation and retrieval, use creation as leading (oh well)
      alias display_name name
      alias display_name= name=
      alias email presenter_email
      alias email= presenter_email=
      alias first_name presenter_first_name
      alias first_name= presenter_first_name=
      alias last_name presenter_last_name
      alias last_name= presenter_last_name=
      alias presenter_image_url photo_url
      alias presenter_image_url= photo_url=
    end
  end
end
