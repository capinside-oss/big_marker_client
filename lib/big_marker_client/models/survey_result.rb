module BigMarkerClient
  module Models
    class SurveyResult < Base
      attr_accessor :question, :email, :response
      attr_reader :date

      def date=(date_str)
        @date = Date.strptime(date_str, "%a, %b %d, %Y") unless date_str.nil? || date_str == ""
      rescue Date::Error
        @date = nil
      end

      def answered?
        !response.nil? && response != ""
      end
    end
  end
end
