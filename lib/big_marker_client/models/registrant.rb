module BigMarkerClient
  module Models
    class Registrant < Base
      attr_accessor :email, :first_name, :last_name, :custom_fields, :enter_url, :bmid, :referral_domain, :source,
                    :tracking_code, :custom_user_id, :time_zone, :country, :earned_certificate,
                    :qualified_for_certificate, :qr_code_value
    end
  end
end
