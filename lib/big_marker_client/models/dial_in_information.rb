module BigMarkerClient
  module Models
    class DialInInformation < Base
      attr_accessor :dial_in_number, :dial_in_id, :dial_in_passcode, :presenter_dial_in_number, :presenter_dial_in_id,
                    :presenter_dial_in_passcode
    end
  end
end
