module BigMarkerClient
  module Models
    class WebinarStats < Base
      attr_accessor :registrants, :revenue, :total_attendees, :page_views, :invited
    end
  end
end
