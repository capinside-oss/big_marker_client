module BigMarkerClient
  module Models
    class Base
      def initialize(hash)
        return unless hash.is_a?(Hash)

        known_attributes = self.class.instance_methods
        hash.each { |k, v| known_attributes.include?(k.to_sym) ? send(:"#{k}=", v) : warn("missing property: #{k}") }
      end

      def to_h
        instance_variables.each_with_object({}) do |var, hash|
          value = instance_variable_get(var)
          hash[key(var)] = if value.is_a?(Array) && !value.empty? && value.first.is_a?(::BigMarkerClient::Models::Base)
                             value.map(&:to_h)
                           elsif value.is_a?(::BigMarkerClient::Models::Base)
                             value.to_h
                           else
                             value
                           end
        end
      end

      private

      def key(var)
        var.to_s.sub("@", "")
      end
    end
  end
end
