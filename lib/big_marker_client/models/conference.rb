module BigMarkerClient
  module Models
    ##
    # for supported values for time_zone please compare [the documentation](https://docs.bigmarker.com/#time-zones)
    class Conference < Base
      attr_accessor :agenda_topics, :audience_open_time, :auto_invite_all_channel_members, :background_image_url,
                    :banner_filter_percentage, :channel_id, :conference_address, :custom_event_id,
                    :disclaimer, :display_language, :duration_minutes, :enable_ie_safari, :enable_knock_to_enter,
                    :enable_registration_email, :enable_twitter, :end_time, :event_type, :exit_url,
                    :fb_open_graph_image_url, :first_admin_enter_time, :id, :manual_end_time, :master_webinar_id,
                    :max_attendance, :meeting_mode, :moderator_open_time, :poll_results, :presenter_exit_url, :privacy,
                    :purpose, :recorded, :recording_iframe, :recording_url, :registration_required_to_view_recording,
                    :review_emails, :room_logo, :send_cancellation_email, :send_reminder_emails_to_presenters,
                    :show_handout_on_page, :show_reviews, :start_time, :time_zone, :title, :type, :webcast_mode,
                    :webhook_url, :webinar_tags, :who_can_watch_recording,
                    ## CCs seems unused?
                    :enable_closed_caption, :cc_original_language, :cc_display_language,
                    # breakout room related
                    :enable_breakout_rooms, :enable_breakout_time, :breakout_time_limit, :breakout_mode,
                    :allow_breakout_mic, :allow_breakout_cam, :allow_breakout_screen, :allow_breakout_whiteboard,
                    :allow_breakout_slide, :main_room_return, :unnassigned_attendee, :unnassigned_presenter,
                    :breakout_room_total,
                    # for conference creation
                    :attendee_advanced_enter_time, :channel_admin_id, :conference_copy_id, :enable_dial_in, :icon,
                    :ondemand_video_url, :presenter_advanced_enter_time, :recurring_start_times,
                    :registration_conf_emails, :room_sub_title, :room_type, :schedule_type,
                    :send_notification_emails_to_presenters, :sub_url, :webinar_format,
                    # webhook related
                    :enable_create_webinar_reg_webhook, :enable_new_reg_webhook,
                    :enable_webinar_start_webhook, :webinar_start_webhook_url, :enable_waiting_room_open_webhook,
                    :waiting_room_open_webhook_url, :enable_webinar_end_webhook, :webinar_end_webhook_url,
                    :enable_live_attendee_data_webhook, :live_attendee_data_webhook_url,
                    :enable_on_demand_viewer_webhook, :on_demand_viewer_webhook_url

      attr_reader :associated_series, :dial_in_information, :preload_files, :presenters, :webinar_stats

      # differences between creation and retrieval, use creation as leading (oh well)
      alias can_view_poll_results poll_results
      alias can_view_poll_results= poll_results=
      alias duration duration_minutes
      alias duration= duration_minutes=
      alias enable_review_emails review_emails
      alias enable_review_emails= review_emails=
      alias language display_language
      alias language= display_language=
      alias copy_webinar_id conference_copy_id
      alias copy_webinar_id= conference_copy_id=
      alias tags webinar_tags
      alias tags= webinar_tags=

      def dial_in_information=(dial_in_hash)
        @dial_in_information = if dial_in_hash.nil? || !dial_in_hash.is_a?(Hash)
                                 nil
                               else
                                 DialInInformation.new(dial_in_hash)
                               end
      end

      def preload_files=(file_array)
        @preload_files = if file_array.nil? || !file_array.is_a?(Array) || file_array.empty?
                           []
                         else
                           file_array.map { |file_hash| PreloadFile.new(file_hash) }
                         end
      end

      def presenters=(presenter_array)
        @presenters = if presenter_array.nil? || !presenter_array.is_a?(Array) || presenter_array.empty?
                        []
                      else
                        presenter_array.map { |presenter_hash| Presenter.new(presenter_hash) }
                      end
      end

      def webinar_stats=(webinar_stats_hash)
        @webinar_stats = if webinar_stats_hash.nil? || !webinar_stats_hash.is_a?(Hash)
                           nil
                         else
                           WebinarStats.new(webinar_stats_hash)
                         end
      end

      def associated_series=(series_array)
        @associated_series = if series_array.nil? || !series_array.is_a?(Array) || series_array.empty?
                               []
                             else
                               # TODO: identify element
                               series_array.map { |series_hash| series_hash.map { |k, v| "#{k}: #{v}" } }
                             end
      end
    end
  end
end
