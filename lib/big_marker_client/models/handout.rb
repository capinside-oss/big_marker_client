module BigMarkerClient
  module Models
    class Handout < Base
      attr_accessor :type, :email, :content, :source_from
      attr_reader :timestamp

      def timestamp=(timestamp)
        @timestamp = DateTime.strptime(timestamp, "%m/%d/%Y %H:%M:%S %z") unless timestamp.nil? || timestamp == ""
      rescue Date::Error
        @timestamp = nil
      end
    end
  end
end
