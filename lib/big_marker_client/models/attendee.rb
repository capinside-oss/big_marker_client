module BigMarkerClient
  module Models
    class Attendee < Base
      attr_accessor :id, :bmid, :conference_id, :email, :first_name, :last_name, :edited_in_room_display_name,
                    :custom_fields, :entered_at, :leaved_at, :total_duration, :engaged_duration, :attendance_monitor,
                    :time_zone, :country, :certificate_of_completion, :chats_count, :qas_count, :polls_count, :polls,
                    :questions, :handouts, :browser_name, :browser_version, :device_name, :start_time, :coupon_used
      attr_reader :view_handout, :download_handout, :survey_results

      def download_handout=(handout_array)
        @download_handout = if handout_array.nil? || !handout_array.is_a?(Array) || handout_array.empty?
                              []
                            else
                              handout_array.map { |handout_hash| Handout.new(handout_hash) }
                            end
      end

      # TODO: handout has same structure as view_handout?

      def survey_results=(results_array)
        @survey_results = if results_array.nil? || !results_array.is_a?(Array) || results_array.empty?
                            []
                          else
                            results_array.map { |result_hash| SurveyResult.new(result_hash) }
                          end
      end

      def view_handout=(handout_array)
        @view_handout = if handout_array.nil? || !handout_array.is_a?(Array) || handout_array.empty?
                          []
                        else
                          handout_array.map { |handout_hash| Handout.new(handout_hash) }
                        end
      end
    end
  end
end
