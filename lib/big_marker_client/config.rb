module BigMarkerClient
  class Config
    class << self
      attr_accessor :logger
      attr_writer :api_key, :base_url, :log, :debug

      def base_url
        @base_url ||= "https://www.bigmarker.com/api/v1"
      end

      def api_key
        @api_key ||= ENV.fetch("BIGMARKER_API_KEY", nil)
      end

      def log
        @log ||= false
      end

      def debug
        @debug ||= false
        if @debug && @logger.nil?
          require "logger"
          @logger ||= defined?(Rails.logger) ? Rails.logger : ::Logger.new($stdout)
        end
        @debug
      end
    end
  end
end
