module BigMarkerClient
  module Api
    module V1
      class Conference < ::BigMarkerClient::Base
        LIST_CONFERENCES              = "/conferences".freeze
        SEARCH_CONFERENCES            = "/conferences/search".freeze
        ASSOCIATED_SERIES_CONFERENCES = "/conferences/get_associated_sessions/{id}".freeze
        RECURRING_CONFERENCES         = "/conferences/recurring/{id}".freeze
        CREATE_CONFERENCE             = "/conferences".freeze
        UPDATE_CONFERENCE             = "/conferences/{id}".freeze
        GET_CONFERENCE                = "/conferences/{id}".freeze
        GET_CONFERENCE_PRESENTERS     = "/conferences/{id}/presenters".freeze
        GET_CONFERENCE_ATTENDEES      = "/conferences/{id}/attendees".freeze
        DELETE_CONFERENCE             = "/conferences/{id}".freeze
        ENTER_CONFERENCE              = "/conferences/enter".freeze

        class << self
          ##
          # lists all conferences, where date range con only be influenced indirectly.
          # @param params [Hash] recognized are:
          # - role: Filter by member role (all*, hosting, attending)
          # - type: Filter by conference type (future*, past, all, multiple_times, future_do_not_include_happening,
          #                                    future_with_24_hour_room, past_with_recording, always_open,
          #                                    happening_now)
          # - query: search in conference title
          # - page: pagination page
          # - page_count: pagination page size (20*), ATTENTION: this is called per_page everywhere else!
          # @return [BigMarkerClient::Models::Conference[]]
          def list(params = {})
            params["page_count"] ||= params["per_page"] if params["per_page"]
            result = get(LIST_CONFERENCES, params)
            return map_to_model_array(result["conferences"]) if result["conferences"]

            result
          end

          ##
          # helper method to retrieve all pages for the #list method
          # @see #list
          def list_all(params = {})
            params["page_count"] ||= params["per_page"] if params["per_page"]
            loop_over(LIST_CONFERENCES, "conferences", ::BigMarkerClient::Models::Conference, params)
          end

          ##
          # search for conferences with specific matching criteria.
          # @param params [Hash] recognized are:
          # - title: search in conference title
          # - start_time: conferences with start time after this date (Unix timestamp)
          # - end_time: conferences with end time before this date (Unix timestamp)
          # - conference_ids: select conferences based on their IDs (comma separated)
          # - presenter_member_ids: select conferences based on their presenters IDs (comma separated)
          # - role: Filter by member role (all*, hosting, attending)
          # - type: Filter by conference type (evergreen_parent evergreen_child recurring_parent recurring_child
          #                                    meetingspace ondemand live_webinar webcast)
          # - page: pagination page
          # - per_page: pagination page size (20*)
          # @return [BigMarkerClient::Models::Conference[]]
          def search(params = {})
            result = post(SEARCH_CONFERENCES, params)
            return map_to_model_array(result["conferences"]) if result["conferences"]

            result
          end

          ##
          # helper method to retrieve all pages for the #search method
          # @see #search
          def search_all(params = {})
            loop_over(SEARCH_CONFERENCES, "conferences", ::BigMarkerClient::Models::Conference, params, :post)
          end

          ##
          # get one conference based on it's ID
          # @param conference_id [String] conference identifier
          # @return [BigMarkerClient::Models::Conference]
          def show(conference_id)
            result = get(replace_path_params(path: GET_CONFERENCE, replacements: { "{id}": conference_id }))
            return ::BigMarkerClient::Models::Conference.new(result["conference"]) if result["conference"]

            result
          end

          ##
          # get series related to a conference.
          # @param conference_id [String] conference identifier
          # @param params [Hash] recognized are:
          # - page: pagination page
          # - per_page: pagination page size (20*)
          # @return [BigMarkerClient::Models::Conference[]]
          def associated_series(conference_id, params = {})
            result = get(replace_path_params(path: ASSOCIATED_SERIES_CONFERENCES,
                                             replacements: { "{id}": conference_id }), params)
            return map_to_model_array(result["conferences"]) if result["conferences"]

            result
          end

          ##
          # helper method to retrieve all pages for the #associated_series method
          # @see #associated_series
          def associated_series_all(conference_id, params = {})
            path = replace_path_params(path: ASSOCIATED_SERIES_CONFERENCES, replacements: { "{id}": conference_id })
            loop_over(path, "conferences", ::BigMarkerClient::Models::Conference, params)
          end

          ##
          # get child conferences of a parent recurring conference.
          # @param conference_id [String] conference identifier
          # @param params [Hash] recognized are:
          # - start_time: conferences with start time after this date (Unix timestamp)
          # - end_time: conferences with end time before this date (Unix timestamp)
          # - page: pagination page
          # - per_page: pagination page size (20*)
          # @return [BigMarkerClient::Models::Conference[]]
          def recurring(conference_id, params = {})
            result = get(replace_path_params(path: RECURRING_CONFERENCES,
                                             replacements: { "{id}": conference_id }), params)
            return map_to_model_array(result["child_conferences"]) if result["child_conferences"]

            result
          end

          ##
          # helper method to retrieve all pages for the #recurring method
          # @see #recurring
          def recurring_all(conference_id, params = {})
            path = replace_path_params(path: RECURRING_CONFERENCES, replacements: { "{id}": conference_id })
            loop_over(path, "child_conferences", ::BigMarkerClient::Models::Conference, params)
          end

          ##
          # update a conference either with a Models::Conference object or single properties as hash
          # @param conference_id [String] conference identifier
          # @param body [Hash or BigMarkerClient::Models::Conference]
          # @return [BigMarkerClient::Models::Conference]
          def update(conference_id, body = {})
            body = body.to_h if body.is_a?(::BigMarkerClient::Models::Conference)
            result = put(replace_path_params(path: UPDATE_CONFERENCE, replacements: { "{id}": conference_id }), body)
            return ::BigMarkerClient::Models::Conference.new(result) if result["id"]

            result
          end

          ##
          # create a conference either with a Models::Conference object or single properties as hash
          # @param body [Hash or BigMarkerClient::Models::Conference]
          # @return [BigMarkerClient::Models::Conference]
          def create(body = {})
            body = body.to_h if body.is_a?(::BigMarkerClient::Models::Base)
            result = post(CREATE_CONFERENCE, body)
            return ::BigMarkerClient::Models::Conference.new(result) if result["id"]

            result
          end

          ##
          # delete a conference
          # @param conference_id [String] conference identifier
          def destroy(conference_id)
            delete(replace_path_params(path: DELETE_CONFERENCE, replacements: { "{id}": conference_id }))
          end

          ##
          # enter a conference towards an enterprise iframe,
          # @param conference_id [String] conference identifier
          # @param name [String] attendee name
          # @param email [String] attendee email
          # @param body [Hash] recognized are:
          # - register_attendee_to_webinar: true/false*
          # - attendee_first_name: First name of the attendee for registration (see register_attendee_to_webinar)
          # - attendee_last_name: Last name of the attendee for registration (see register_attendee_to_webinar)
          # - custom_user_id: an external user id for matching
          # - role: the role of the user that is entering the webinar (attendee*, moderator, presenter)
          # - exit_uri: URL that attendees will be redirected to after the webinar
          # @return [Hash] enter_token: auth token for conference, enter_uri: URL to enter conference
          def enter(conference_id, name, email, body = {})
            body.merge!(id: conference_id, attendee_name: name, attendee_email: email)
            post(ENTER_CONFERENCE, body)
          end

          ##
          # Get all presenters of a conference.
          # @param conference_id [String] conference identifier
          # @return [BigMarkerClient::Models::Presenter[]]
          def presenters(conference_id)
            result = get(replace_path_params(path: GET_CONFERENCE_PRESENTERS, replacements: { "{id}": conference_id }))
            if result["presenters"]
              return map_to_model_array(result["presenters"], ::BigMarkerClient::Models::Presenter)
            end

            result
          end

          ##
          # Get all registered attendees of a conference. Recognized @params are:
          # @param conference_id [String] conference identifier
          # @param params [Hash] recognized are:
          # - page: pagination page
          # - per_page: pagination page size (20*)
          # @return [BigMarkerClient::Models::Attendee[]]
          def attendees(conference_id, params = {})
            result = get(replace_path_params(path: GET_CONFERENCE_ATTENDEES,
                                             replacements: { "{id}": conference_id }), params)
            return map_to_model_array(result["attendees"], ::BigMarkerClient::Models::Attendee) if result["attendees"]

            result
          end

          ##
          # helper method to retrieve all pages for the #attendees method
          # @see #attendees
          def attendees_all(conference_id, params = {})
            path = replace_path_params(path: GET_CONFERENCE_ATTENDEES, replacements: { "{id}": conference_id })
            loop_over(path, "attendees", ::BigMarkerClient::Models::Attendee, params)
          end

          private

          def map_to_model_array(hash_array, model_class = ::BigMarkerClient::Models::Conference)
            hash_array.map { |hash| model_class.new(hash) }
          end
        end
      end
    end
  end
end
