module BigMarkerClient
  module Api
    module V1
      class Conference
        class Registration < ::BigMarkerClient::Base
          LIST_REGISTRANTS              = "/conferences/registrations/{id}".freeze
          LIST_REGISTRANTS_WITH_FIELDS  = "/conferences/registrations_with_fields/{id}".freeze
          CREATE_REGISTRANT             = "/conferences/register".freeze
          DELETE_REGISTRANT             = "/conferences/register".freeze
          CHECK_REGISTRANT_STATUS       = "/conferences/{id}/check_registration_status".freeze

          class << self
            ##
            # list all registrants to a conference.
            # @param conference_id [String] conference identifier
            # @param params [Hash] recognized are:
            # - page: pagination page
            # - per_page: pagination page size (20*)
            # @return [BigMarkerClient::Models::Registrant[]]
            def list(conference_id, params = {})
              result = get(replace_path_params(path: LIST_REGISTRANTS, replacements: { "{id}": conference_id }), params)
              return map_to_model_array(result["registrations"]) if result["registrations"]

              result
            end

            ##
            # helper method to retrieve all pages for the #list method
            # @see #list
            def list_all(conference_id, params = {})
              path = replace_path_params(path: LIST_REGISTRANTS, replacements: { "{id}": conference_id })
              loop_over(path, "registrations", ::BigMarkerClient::Models::Registrant, params)
            end

            ##
            # list all registrants to a conference with custom fields.
            # @param conference_id [String] conference identifier
            # @param params [Hash] recognized are:
            # - page: pagination page
            # - per_page: pagination page size (20*)
            # @return [BigMarkerClient::Models::Registrant[]]
            def list_with_fields(conference_id, params = {})
              result = get(replace_path_params(path: LIST_REGISTRANTS_WITH_FIELDS,
                                               replacements: { "{id}": conference_id }), params)
              return map_to_model_array(result["registrations"]) if result["registrations"]

              result
            end

            ##
            # helper method to retrieve all pages for the #list_with_fields method
            # @see #list_with_fields
            def list_all_with_fields(conference_id, params = {})
              path = replace_path_params(path: LIST_REGISTRANTS_WITH_FIELDS, replacements: { "{id}": conference_id })
              loop_over(path, "registrations", ::BigMarkerClient::Models::Registrant, params)
            end

            ##
            # registers a participant to a conference.
            # @param conference_id [String] conference identifier
            # @param email [String] attendee email
            # @param first_name [String] attendee first name
            # @param last_name [String] attendee last name
            # @param params [Hash] recognized are:
            # - temporary_password:	a temporary password for registrant
            # - custom_fields: uRL encoded JSON object, keys are custom_field ids.
            # - utm_bmcr_source: registrant’s UTM source code
            # - custom_user_id: external custom user id
            # @return [String] URL for the participant to enter the conference
            def register(conference_id, email, first_name, last_name, params = {})
              create_params = { id: conference_id, email: email, first_name: first_name, last_name: last_name }
                              .merge(params)
              result = post(CREATE_REGISTRANT, create_params)
              return result["conference_url"] if result["conference_url"]

              result
            end

            ##
            # remove a participant from a conference
            # @param conference_id [String] conference identifier
            # @param email [String] attendee email
            def unregister(conference_id, email)
              delete(DELETE_REGISTRANT, { id: conference_id, email: email })
            end

            ##
            # check if a person is already registered to a conference. Recognized @params are
            # @param conference_id [String] conference identifier
            # @param params [Hash] recognized are:
            # - bmid: a unique big_marker id of a attendee
            # - email: the email of an attendee
            # either of these two are required
            # @return [String] status (registered. not registered)
            def check_status(conference_id, params = {})
              result = get(replace_path_params(path: CHECK_REGISTRANT_STATUS, replacements: { "{id}": conference_id }),
                           params)
              return result["registration_status"] if result["registration_status"]

              result
            end

            private

            def map_to_model_array(hash_array, model_class = ::BigMarkerClient::Models::Registrant)
              hash_array.map { |hash| model_class.new(hash) }
            end
          end
        end
      end
    end
  end
end
