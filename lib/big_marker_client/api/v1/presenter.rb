module BigMarkerClient
  module Api
    module V1
      class Presenter < ::BigMarkerClient::Base
        LIST_PRESENTER_CONFERENCES = "/presenters/{id}/list_conferences".freeze
        CREATE_PRESENTER           = "/presenters".freeze
        UPDATE_PRESENTER           = "/presenters/{id}".freeze
        DELETE_PRESENTER           = "/presenters/{id}".freeze

        class << self
          ##
          # lists all conferences of a presenter
          # @param member_id [String] BigMarker member id (BMID)
          # @param channel_id [String] BigMarker channel id
          # @return [BigMarkerClient::Models::Conference[]]
          def list_conferences(member_id, channel_id)
            result = get(replace_path_params(path: LIST_PRESENTER_CONFERENCES, replacements: { "{id}": member_id }),
                         { channel_id: channel_id })
            if result["conferences"]
              return result["conferences"].map { |conference| ::BigMarkerClient::Models::Conference.new(conference) }
            end

            result
          end

          ##
          # update a presenter to a conference either with a Models::Presenter object or single properties as hash
          # @param member_id [String] BigMarker member identifier (BMID)
          # @param body [Hash or BigMarkerClient::Models::Presenter]
          # @return [BigMarkerClient::Models::Presenter]
          def update(member_id, body = {})
            body = body.to_h if body.is_a?(::BigMarkerClient::Models::Presenter)
            result = put(replace_path_params(path: UPDATE_PRESENTER, replacements: { "{id}": member_id }), body)
            return ::BigMarkerClient::Models::Presenter.new(result["presenter"]) if result["presenter"]

            result
          end

          ##
          # add a presenter to a conference either with a Models::Presenter object or single properties as hash
          # @param conference_id [String] conference identifier
          # @param body [Hash or BigMarkerClient::Models::Presenter]
          # @return [BigMarkerClient::Models::Presenter]
          def create(conference_id, body = {})
            body = body.to_h if body.is_a?(::BigMarkerClient::Models::Base)
            body["conference_id"] = conference_id
            result = post(CREATE_PRESENTER, body)
            return ::BigMarkerClient::Models::Presenter.new(result["presenter"]) if result["presenter"]

            result
          end

          ##
          # remove a presenter from a conference
          # @param member_id [String] BigMarker member identifier (BMID)
          def destroy(member_id)
            delete(replace_path_params(path: DELETE_PRESENTER, replacements: { "{id}": member_id }))
          end
        end
      end
    end
  end
end
