require "faraday"

module BigMarkerClient
  class Base
    DEFAULT_PER_PAGE_SIZE = 25

    class << self
      def post(path, body = {})
        request(verb: :post, path: path, params: body)
      end

      def put(path, body = {})
        request(verb: :put, path: path, params: body)
      end

      def patch(path, body = {})
        request(verb: :patch, path: path, params: body)
      end

      def delete(path, body = {})
        request(verb: :delete, path: path, params: body)
      end

      def get(path, params = {})
        request(verb: :get, path: path, params: params)
      end

      protected

      def replace_path_params(path:, replacements: {})
        new_path = path.dup
        replacements.each { |k, v| new_path.gsub!(k.to_s, v) }
        new_path
      end

      def loop_over(path, field, model_class, params = {}, method = :get)
        page = 1
        results = []
        loop do
          params["page"] = page
          result = send(method, path, params)
          results += map_to_model_array(result[field], model_class) if result[field]
          break if break?(results: results, result: result, page: page, page_size: page_size(params))

          page += 1
        end
        results
      end

      private

      def request(path:, verb: :get, params: {})
        check_preconditions(verb, path)
        params = stringify_keys(params)

        params = params.to_json unless %w[get delete].include?(verb.to_s)
        @http_client ||= HttpClient.new
        response = @http_client.connection.send(verb.to_s, base_url(path), params)
        return parse_body(response.body) if response.success?

        response.body
      end

      def check_preconditions(verb, path)
        raise ArgumentError, "http_method, path or api key is missing" if verb.nil? || path.nil? || Config.api_key.nil?
        raise ArgumentError, "unsupported http_method: #{verb}" unless %w[post put patch delete get].include?(verb.to_s)
      end

      def stringify_keys(hash)
        hash_symbol_keys = hash.keys.select { |key| key.is_a?(Symbol) }
        hash_symbol_keys.each do |key|
          hash[key.to_s] = hash[key]
          hash.delete(key)
        end
        hash
      end

      def base_url(path)
        Config.base_url + (path.start_with?("/") ? path : "/#{path}")
      end

      def parse_body(body)
        return nil if body.strip == ""

        json = JSON.parse(body)
        Config.logger.debug(json) if Config.debug
        json
      rescue JSON::ParserError
        raise ResponseError, "invalid response"
      end

      def page_size(params)
        params["page_count"] || params.fetch("per_page", DEFAULT_PER_PAGE_SIZE)
      end

      ##
      # BigMarker API is a total mess that won't return total_pages or total_entries on all request types so we have to
      # get creative
      def break?(results:, result:, page:, page_size:)
        return true if break_on_full_metadata?(results: results, result: result, page: page) ||
                       break_on_partial_metadata?(results: results, result: result, page: page)

        # rubocop:disable Style/ZeroLengthPredicate
        results.length.zero? || (results.length % page_size) != 0 || (results.length.to_f / page_size) < page
        # rubocop:enable Style/ZeroLengthPredicate
      end

      def break_on_full_metadata?(results:, result:, page:)
        if !result["total_pages"].nil? && !total_count(result).nil?
          return (page >= result["total_pages"].to_i) || (results.length >= total_count(result).to_i)
        end

        false
      end

      def break_on_partial_metadata?(results:, result:, page:)
        return page >= result["total_pages"].to_i unless result["total_pages"].nil?
        return results.length >= total_count(result).to_i unless total_count(result).nil?

        false
      end

      ##
      # conferences#list is a total mess as requests require `page_count` instead of `per_page` as everywhere else and
      # ti will return `total_count` instead of `total_entries` compared to the rest
      def total_count(response)
        response["total_entries"] || response["total_count"]
      end
    end
  end
end
