module BigMarkerClient
  class HttpClient
    attr_reader :connection

    def initialize
      @connection = Faraday.new(url: Config.base_url) do |faraday|
        faraday = headers(faraday)
        configure_logging(faraday) if Config.log
      end
    end

    private

    def headers(adapter)
      adapter.headers["Content-Type"] = "application/json"
      adapter.headers["API-KEY"] = Config.api_key unless Config.api_key.nil?
      adapter
    end

    def configure_logging(adapter)
      adapter.response :logger do |logger|
        logger.instance_variable_get(:@options)[:log_level] = :debug if Config.debug
        logger.filter(/password=([^&]+)/, "password=[FILTERED]")
        logger.filter(/API-KEY: "(\w*)"/, "API-KEY: [FILTERED]")
      end
      adapter
    end
  end
end
