module BigMarkerClient
  module TestSupport
    FACTORY_PATH = File.expand_path("../../spec/factories", __dir__)
    MINUTES = 60
    DAYS = 24 * 60 * 60
  end
end
