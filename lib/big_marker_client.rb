require "json"

module BigMarkerClient
  class Error < StandardError; end
  class ConnectionError < StandardError; end
  class ClientError < ConnectionError; end
  class ServerError < ConnectionError; end
  class ResponseError < ConnectionError; end
  class NotFound < ResponseError; end

  autoload :Base, "big_marker_client/base"
  autoload :Config, "big_marker_client/config"
  autoload :HttpClient, "big_marker_client/http_client"
  autoload :Version, "big_marker_client/version"

  module Api
    module V1
      autoload :Conference, "big_marker_client/api/v1/conference"
      class Conference
        autoload :Registration, "big_marker_client/api/v1/conference/registration"
      end
      autoload :Presenter, "big_marker_client/api/v1/presenter"
    end
  end

  module Models
    autoload :Attendee, "big_marker_client/models/attendee"
    autoload :Base, "big_marker_client/models/base"
    autoload :Conference, "big_marker_client/models/conference"
    autoload :DialInInformation, "big_marker_client/models/dial_in_information"
    autoload :Handout, "big_marker_client/models/handout"
    autoload :PreloadFile, "big_marker_client/models/preload_file"
    autoload :Presenter, "big_marker_client/models/presenter"
    autoload :Registrant, "big_marker_client/models/registrant"
    autoload :SurveyResult, "big_marker_client/models/survey_result"
    autoload :WebinarStats, "big_marker_client/models/webinar_stats"
  end
end
