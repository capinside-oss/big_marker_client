# BigMarkerClient

This is a lightweight wrapper for rails around the [BigMarker API](https://docs.bigmarker.com/). You will need an API key to be able to use it, set it in the configuration.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'big_marker_client'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install big_marker_client

## Usage

As this is a lightweight wrapper around the BigMarker API, mostly refer to [their documentation](https://docs.bigmarker.com/). Most notably the creation of conferences will require a brief look into their requirements.

To be able to connect to BigMarker you need to set the `BigMarkerClient::Config.api_key`, as a default this will look for an environment variable `BIGMARKER_API_KEY` to read the key from.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Note 

Only a limited set of all API functions are currently implemented, however the existing framework makes implementing further functions extremely lightweight and easy.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/capinside-oss/big_marker_client. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/capinside/big_marker_client/-/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT). BigMarker is a registered trademark of BigMarker.com LLC., Chicago Illinois USA.

## Code of Conduct

Everyone interacting in the BigMarkerClient project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/capinside-oss/big_marker_client/-/blob/main/CODE_OF_CONDUCT.md).
