# Changelog

## [0.1.12] - unreleased

-

## [0.1.11] - 2024-04-29

- add `qr_code_value` to registrant 

## [0.1.10] - 2024-01-09

- add `presenter_exit_url` and `fb_open_graph_image_url` to conference
- add `earned_certificate` and `qualified_for_certificate` to registrant

## [0.1.9] - 2023-03-28

- add conference fields `tags`, `banner_filter_percentage`
- add attendee fields `start_time`, `coupon_used`
- add webhooks related fields

## [0.1.8] - 2022-05-20

- loosen faraday dependency

## [0.1.7] - 2022-05-20

- fix return types for update and create are different from show
- fix attendee duration format (not integer minutes but time string)
- add attendee field `certificate_of_completion`
- add participant fields `enter_as_attendee`, `remove_bigmarker_contact_option`, `show_on_networking_center`,
  `show_chat_icon_in_booth_or_session_room`
- better error handling of date fields
- better handling of attendees `SurveyResult`
- bundle update

## [0.1.6] - 2022-01-19

- fix recurring method to return "child_conferences"

## [0.1.5] - 2022-01-19 (broken, don't use!)

- refactor HTTP client into a separate class
- fix looping if no meta-data is returned

## [0.1.4] - 2022-01-18

- add new properties to attendees and add handout model and factory

## [0.1.3] - 2022-01-11

- add helper methods to iterate over all pages for paginated requests

## [0.1.2] - 2022-01-07

- actually package the factories into the gem

## [0.1.1] - 2022-01-07

- additional properties `time_zone` and `country` on registrant
- factories for external testing

## [0.1.0] - 2021-12-10

- Initial release
